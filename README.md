# WallKickers

Spillet er en enkel "replica" av det eksisterende spillet "Wallkickers" og er laget i sammenheng med en innleveringsoppgave på UIB. 

Spillet benytter bare én knapp. "mellomrom"-tasten brukes til å navigere menyen og kontrollere spillet. Trykk for å hoppe, hold inne for å hoppe høyere og trykk en gang til mens i lufta for å bytte retning/"backflippe".

Første versjon av spillet introduserer 11 leveler, inkludert "startlevel", spillet vil velge tilfeldig blant disse. Disse levelene har forskjellige strukturer bygd opp av forskjellige "Obstacles"/vegger. 

Wall - vanlig vegg.
SpikeWall - dreper spilleren.
BlinkWall - vegg som forsvinner og kommer tilbake.
ElectricWall - vegg som kan bytter mellom dødelig og å fungere som vanlig vegg.
BounceWall - vegg utløser et hopp automatisk, slik at spilleren spretter av den.

Målet med spillet er å komme så høyt som mulig. Spillet lagrer Highscore slik at man har insentiv til å nå nye høyder.

Spillet er laget med objektorientert programmering og bruk av model-view-controller konseptet.

Lenke til det orginale spillet "WallKicker": https://www.wallkickers.com/

Bakgrunnsbildet til spillet er et gratis bilde hentet fra: https://edermunizz.itch.io/free-pixel-art-forest

Spill figuren er laget i et gratis online-program: https://www.pixilart.com/ (med inspirasjon fra det orginale wall kickers spillet)

Video demo av spillet: https://youtu.be/kUnM1ThTlzk 

NB! Highscore er ikke testet med installer, funker hvis "user.dir" er "sem-2-mathias-ness" 

Laget av Mathias Hop Ness